package com.victor.loading.example;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

public class MainActivity extends Ability implements Component.ClickedListener {
    private static String TAG = "MainActivity";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Button rotate = (Button) findComponentById(ResourceTable.Id_btn_RotateLoading);
        Button book = (Button) findComponentById(ResourceTable.Id_btn_BookLoading);
        Button cradle = (Button) findComponentById(ResourceTable.Id_btn_NewtonCradleLoading);

        rotate.setClickedListener(this);
        book.setClickedListener(this);
        cradle.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        Class<?> activity = null;
        switch (component.getId()) {
            case ResourceTable.Id_btn_RotateLoading:
                activity = RotateActivity.class;
                break;
            case ResourceTable.Id_btn_BookLoading:
                activity = BookActivity.class;
                break;
            case ResourceTable.Id_btn_NewtonCradleLoading:
                activity = NewtonCradleActivity.class;
                break;
            default:
                LogUtil.debug(TAG, "No button match found");
                break;
        }
        if (null != activity) {
            Operation operation = new Intent.OperationBuilder()
                    .withBundleName("com.victor.loading.example")
                    .withAbilityName(activity)
                    .build();
            Intent intent = new Intent();
            intent.setOperation(operation);
            startAbility(intent);
        }
    }
}
