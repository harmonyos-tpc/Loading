package com.victor.loading.example;

import com.victor.loading.rotate.RotateLoading;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

/**
 * @author Victor
 *         create at 15/7/28 21:37
 */
public class RotateActivity extends Ability {
    private RotateLoading rotateLoading;
    private Button button;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_activity_rotate);

        rotateLoading = (RotateLoading) findComponentById(ResourceTable.Id_rotateloading);
        button = (Button) findComponentById(ResourceTable.Id_button);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (rotateLoading.isStart()) {
                    rotateLoading.stop();
                    button.setText(ResourceTable.String_start);
                } else {
                    rotateLoading.start();
                    button.setText(ResourceTable.String_stop);
                }
            }
        });
    }
}
