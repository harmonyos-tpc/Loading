package com.victor.loading.example;

import com.victor.loading.book.BookLoading;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

/**
 * @author Victor
 *         create at 15/7/28 21:31
 */
public class BookActivity extends Ability {
    private BookLoading bookLoading;
    private Button button;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_activity_book);
        bookLoading = (BookLoading) findComponentById(ResourceTable.Id_bookloading);
        button = (Button) findComponentById(ResourceTable.Id_button);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (bookLoading.isStart()) {
                    button.setText(ResourceTable.String_start);
                    bookLoading.stop();
                } else {
                    button.setText(ResourceTable.String_stop);
                    bookLoading.start();
                }
            }
        });
    }
}
