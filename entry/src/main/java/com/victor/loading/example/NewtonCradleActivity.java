package com.victor.loading.example;

import com.victor.loading.newton.NewtonCradleLoading;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

/**
 * @author Yan
 *         create at 15/8/9 22:09
 */
public class NewtonCradleActivity extends Ability {
    private NewtonCradleLoading newtonCradleLoading;
    private Button button;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_activity_newton_cradle);

        newtonCradleLoading = (NewtonCradleLoading) findComponentById(ResourceTable.Id_newton_cradle_loading);
        button = (Button) findComponentById(ResourceTable.Id_button);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (newtonCradleLoading.isStart()) {
                    button.setText(ResourceTable.String_start);
                    newtonCradleLoading.stop();
                } else {
                    button.setText(ResourceTable.String_stop);
                    newtonCradleLoading.start();
                }
            }
        });
    }
}
