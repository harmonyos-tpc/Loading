package com.victor.loading.example;

import com.victor.loading.newton.NewtonCradleLoading;
import com.victor.loading.rotate.RotateLoading;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.utils.Color;
import ohos.app.Context;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    private RotateLoading rotateLoading;
    private NewtonCradleLoading newtonCradleLoading;
    private Context context;

    /**
     * setup for testCases
     */
    @Before
    public void setup() {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        rotateLoading = new RotateLoading(context);
        newtonCradleLoading = new NewtonCradleLoading(context);
    }

    /**
     * test case for testLoadingColor
     */
    @Test
    public void testLoadingColorForRotateLoading() {
        rotateLoading.setLoadingColor(Color.GREEN.getValue());
        assertEquals(Color.GREEN.getValue(), rotateLoading.getLoadingColor());
    }
}