# Loading

Loading is a project with kinds of openharmony loading view.

## Loading includes :
* RotateLoading
* BookLoading
* NewtonCradleLoading

# Usage Instructions
Loading can be used by instantiating the layout elements as shown below :

### RotateLoading

```
    <com.victor.loading.rotate.RotateLoading
        ohos:id="$+id:rotateloading"
        ohos:height="100vp"
        ohos:width="100vp"
        ohos:center_in_parent="true"
        app:loading_speed="15"
        app:loading_width="15"/>
```

### BookLoading

```
    <com.victor.loading.book.BookLoading
        ohos:id="$+id:bookloading"
        ohos:height="100vp"
        ohos:width="150vp"
        ohos:center_in_parent="true"/>
```
### NewtonCradleLoading

```
    <com.victor.loading.newton.NewtonCradleLoading
        ohos:id="$+id:newton_cradle_loading"
        ohos:height="150fp"
        ohos:width="match_content"
        ohos:center_in_parent="true"/>
```

# Installation Instructions

1.For using Loading module in sample app, include the source code and add the below dependencies in entry/build.gradle to generate hap/lib.har.

```
dependencies {
    implementation project(':lib')
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    testCompile 'junit:junit:4.12'
    }
```

2.For using Loading in separate application using har file, add the har file in the entry/libs folder and add the dependencies in entry/build.gradle file.

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    testCompile 'junit:junit:4.12'
}
```

3. Remote repository dependency has not been released and will be added in the future.


# License
```
Copyright 2015 yankai-victor

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this work except in compliance with the License. You may obtain a copy of the License in the LICENSE file, or at:

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
```