package com.victor.loading.book;

import com.victor.loading.LogUtil;
import com.victor.loading.ResourceTable;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.ThreeDimView;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * Page
 * Created by Victor on 15/7/28.
 */
public class PageView extends Component implements Component.DrawTask {
    private static String TAG = "PageView";
    private Paint paint;
    private Path path;

    private int width;
    private int height;
    private float padding;
    private int border;
    private Context mContext;
    private float time;
    private final int TRANSLATE_VALUE = 100;

    public PageView(Context context) {
        super(context);
        mContext = context;
        initView();
    }

    public PageView(Context context, AttrSet attrs) {
        super(context, attrs);
        mContext = context;
        initView();
    }

    public PageView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView();
    }

    private void initView() {
        padding = dpToPx(mContext, 15);
        border = dpToPx(mContext, 10);

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(dpToPx(mContext, 5));
        path = new Path();
        addDrawTask(this::onDraw);
        invalidate();
    }

    public void updateValue(float val) {
        time = val;
        invalidate();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        width = getWidth();
        height = getHeight();

        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL_STYLE);
        paint.setColor(new Color(getColor(ResourceTable.Color_book_loading_page)));

        paint.setAntiAlias(true);
        paint.setColor(new Color(getColor(ResourceTable.Color_book_loading_book)));
        paint.setStyle(Paint.Style.STROKE_STYLE);
        float offset = border / 4;
        path.moveTo(width / 2, padding + offset);
        path.lineTo(width - padding - offset, padding + offset);
        path.lineTo(width - padding - offset, height - padding - offset);
        path.lineTo(width / 2, height - padding - offset);
        canvas.drawPath(path, paint);

        ThreeDimView threeDimView = new ThreeDimView();
        canvas.translate(component.getWidth() / 2, component.getHeight() - padding - offset - TRANSLATE_VALUE);
        threeDimView.rotateY(-time * 180);
        threeDimView.applyToCanvas(canvas);
        canvas.translate(-component.getWidth() / 2, -component.getHeight() + padding + offset + TRANSLATE_VALUE);

        canvas.drawRect(width / 2, padding + offset, width - padding - offset, height - padding - offset, paint);
    }

    public int dpToPx(Context context, float dpVal) {
        if (dpVal <= 0) {
            return 0;
        }
        return (int) (context.getResourceManager().getDeviceCapability().screenDensity / 160 * dpVal);
    }

    /**
     * get the Color value
     *
     * @param id the id
     * @return get the String value
     */
    public int getColor(int id) {
        int result = 0;
        try {
            result = getResourceManager().getElement(id).getColor();
        } catch (IOException e) {
            LogUtil.error(TAG, "getColor -> IOException");
        } catch (NotExistException e) {
            LogUtil.error(TAG, "getColor -> NotExistException");
        } catch (WrongTypeException e) {
            LogUtil.error(TAG, "getColor -> WrongTypeException");
        }
        return result;
    }
}
