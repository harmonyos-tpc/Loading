package com.victor.loading.book;

import com.victor.loading.LogUtil;
import com.victor.loading.ResourceTable;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * The background
 * Created by Victor on 15/7/28.
 */
public class BookView extends Component implements Component.DrawTask {
    private static String TAG = "BookView";
    private Paint paint;
    private int width;
    private int height;

    public BookView(Context context) {
        super(context);
        initView();
    }

    public BookView(Context context, AttrSet attrs) {
        super(context, attrs);
        initView();
    }

    public BookView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        paint = new Paint();
        try {
            paint.setColor(new Color(getResourceManager().getElement(ResourceTable.Color_book_loading_book)
                    .getColor()));
            paint.setStrokeWidth(dpToPx(mContext, 10));
            paint.setAntiAlias(true);
            paint.setStyle(Paint.Style.STROKE_STYLE);
        } catch (Exception exe) {
            LogUtil.error(TAG, "IOException in initView " + exe.toString());
        }
        addDrawTask(this);
        invalidate();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        width = component.getWidth();
        height = component.getHeight();
        canvas.drawRect(0, 0, width, height, paint);
    }

    public int dpToPx(Context context, float dpVal) {
        if (dpVal <= 0) {
            return 0;
        }
        return (int) (context.getResourceManager().getDeviceCapability().screenDensity / 160 * dpVal);
    }
}
