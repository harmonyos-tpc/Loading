/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.victor.loading.customAnimation;

/**
 * Class to get animated values
 */
public class AnimatorValueUtils {
    /**
     * API to get animated value
     *
     * @param frac_val float values animation
     * @param values change in animation based on these values
     * @return calculated value based on frac_val and values
     */
    public static float getAnimValues(float frac_val, float... values) {
        if (values == null || values.length == 0) {
            return 0;
        }
        if (values.length == 1) {
            return values[0] * frac_val;
        } else {
            if (frac_val == 1) {
                return values[values.length - 1];
            }
            float oneFrac = 1f / (values.length - 1);
            float offFrac = 0;
            for (int i = 0; i < values.length - 1; i++) {
                if (offFrac + oneFrac >= frac_val) {
                    return values[i] + (frac_val - offFrac) * (values.length - 1) * (values[i + 1] - values[i]);
                }
                offFrac += oneFrac;
            }
        }
        return 0;
    }
}
