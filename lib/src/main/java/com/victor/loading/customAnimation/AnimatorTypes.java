/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.victor.loading.customAnimation;

/**
 * Animator types class
 */
public class AnimatorTypes {
    /**
     * constant for move by x
     */
    public static final int MOVE_BY_X = 2;

    /**
     * To check type of animator
     * @param animatorType type
     * @return its type
     */
    public static boolean checkAnimatorType(int animatorType) {
        return animatorType == MOVE_BY_X;
    }
}
