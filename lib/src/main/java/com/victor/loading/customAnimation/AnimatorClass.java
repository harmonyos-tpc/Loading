/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.victor.loading.customAnimation;

import com.victor.loading.LogUtil;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * AnimatorClass that changes animation based on values
 */
public class AnimatorClass extends AnimatorValue implements Animator.StateChangedListener {
    private static String TAG = "AnimatorClass";
    private WeakReference<Component> targetComp;
    private ArrayList<AnimatorSetterGetter> valueArraylist;
    private long mDuration = 1000;
    private int mAnimatorType = -1;
    private int unitTime = 0;
    private AnimatorProperty animProperty;
    private int runningIndex = 0;

    private AnimatorClass(Component component, int animatorType, long duration, List<AnimatorSetterGetter> values) {
        if (!AnimatorTypes.checkAnimatorType(animatorType)) {
            LogUtil.error(TAG, "Enter a valid animatorType.");
        }
        if (duration <= 0) {
            LogUtil.error(TAG,"The duration must be greater than 0.");
        }
        mDuration = duration;
        unitTime = Math.round(duration / values.size());
        targetComp = new WeakReference<>(component);
        mAnimatorType = animatorType;
        valueArraylist = new ArrayList<>(values);
        setDuration(duration);
        setStateChangedListener(this);
    }

    /**
     * Initialize animation collection
     */
    private void configAnimator() {
        if (runningIndex < valueArraylist.size()) {
            animProperty =
                    AnimationBuilder.generateAnimByType(
                            targetComp.get(), mAnimatorType, valueArraylist.get(runningIndex), unitTime, 0, this);
            if (animProperty != null) {
                animProperty.start();
            }
        }
    }

    /**
     * Generate AnimatorClass according to the incoming parameters
     * @param targetComponent component target
     * @param animatorType type of animation
     * @param duration duration in long
     * @param values animation changing values
     * @return AnimatorClass
     */
    public static AnimatorClass ofFloat(Component targetComponent, int animatorType, long duration, Float... values) {
            List<Float> tempList = Arrays.asList(values);
            ArrayList<AnimatorSetterGetter> targetList = new ArrayList<>();
            for (Float number : tempList) {
                targetList.add(new AnimatorSetterGetter(number));
            }
            return new AnimatorClass(targetComponent, animatorType, duration, targetList);
    }

    @Override
    public void start() {
        configAnimator();
    }

    @Override
    public void onStart(Animator animator) {}

    @Override
    public void onStop(Animator animator) {}

    @Override
    public void onCancel(Animator animator) {}

    @Override
    public void onEnd(Animator animator) {
        runningIndex++;
        configAnimator();
    }

    @Override
    public void onPause(Animator animator) {}

    @Override
    public void onResume(Animator animator) {}
}
