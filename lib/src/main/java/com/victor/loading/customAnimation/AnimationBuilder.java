/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.victor.loading.customAnimation;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

/**
 * Animation builder class
 */
public class AnimationBuilder {
    /**
     * Generate attribute animation based on incoming parameters
     *
     * @param target        target component
     * @param animatorType  type of animation required
     * @param animatorValue animation values
     * @param duration      duration in long
     * @param delay         delay required
     * @param listener      listener for changes
     * @return property animation
     */
    public static AnimatorProperty generateAnimByType(
            Component target,
            int animatorType,
            AnimatorSetterGetter animatorValue,
            int duration,
            int delay,
            Animator.StateChangedListener listener) {
        AnimatorProperty animatorProperty = new AnimatorProperty(target);
        animatorProperty.setCurveType(Animator.CurveType.LINEAR);
        float value = animatorValue.getMainValue();
        type(animatorProperty, animatorType, animatorValue, value);
        animatorProperty.setDuration(duration).setDelay(delay).setStateChangedListener(listener);
        return animatorProperty;
    }

    private static void type(
            AnimatorProperty animatorProperty, int animatorType, AnimatorSetterGetter animatorValue, float value) {
        if (animatorType == AnimatorTypes.MOVE_BY_X) {
            animatorProperty.moveByX(value);
        }
    }
}
