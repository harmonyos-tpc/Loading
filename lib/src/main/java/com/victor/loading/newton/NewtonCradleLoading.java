package com.victor.loading.newton;

import com.victor.loading.ResourceTable;
import com.victor.loading.customAnimation.AnimatorClass;
import com.victor.loading.customAnimation.AnimatorTypes;
import com.victor.loading.customAnimation.AnimatorValueUtils;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

/**
 * @author Victor
 *         create at 15/8/9
 */
public class NewtonCradleLoading extends DirectionalLayout {
    private CradleBall cradleBallOne;
    private CradleBall cradleBallTwo;
    private CradleBall cradleBallThree;
    private CradleBall cradleBallFour;
    private CradleBall cradleBallFive;

    private static final int DURATION = 1500;
    private static final float PIVOT_X = 20f;
    private static final float PIVOT_Y = 50f;
    private static final int DEGREE = 50;
    private static final int DELAY = 500;

    private boolean isStart = false;

    public NewtonCradleLoading(Context context) {
        super(context);
        initView(context);
    }

    public NewtonCradleLoading(Context context, AttrSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public NewtonCradleLoading(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_newton_cradle_loading, this, true);
        cradleBallOne = (CradleBall) findComponentById(ResourceTable.Id_ball_one);
        cradleBallTwo = (CradleBall) findComponentById(ResourceTable.Id_ball_two);
        cradleBallThree = (CradleBall) findComponentById(ResourceTable.Id_ball_three);
        cradleBallFour = (CradleBall) findComponentById(ResourceTable.Id_ball_four);
        cradleBallFive = (CradleBall) findComponentById(ResourceTable.Id_ball_five);
    }

    private void ballOneAnim(){
        AnimatorValue av = new AnimatorValue();
        av.setDuration(DURATION);
        av.setDelay(DELAY);
        av.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float val) {
                cradleBallOne.setRotation(AnimatorValueUtils.getAnimValues(val, DEGREE, 0));
                cradleBallOne.setPivotX(AnimatorValueUtils.getAnimValues(val, PIVOT_X, PIVOT_X));
                cradleBallOne.setPivotY(AnimatorValueUtils.getAnimValues(val, -PIVOT_Y, -PIVOT_Y));
            }
        });
        av.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) { }

            @Override
            public void onStop(Animator animator) { }

            @Override
            public void onCancel(Animator animator) { }

            @Override
            public void onEnd(Animator animator) {
                if(isStart) {
                    AnimatorClass.ofFloat(cradleBallTwo, AnimatorTypes.MOVE_BY_X, DURATION/3, 0f, 10f,0f,-10f,0f,10f,0f,-10f,0f).start();
                    AnimatorClass.ofFloat(cradleBallThree, AnimatorTypes.MOVE_BY_X, DURATION/3, 0f, 10f,0f,-10f,0f,10f,0f,-10f,0f).start();
                    AnimatorClass.ofFloat(cradleBallFour, AnimatorTypes.MOVE_BY_X, DURATION/3, 0f, 10f,0f,-10f,0f,10f,0f,-10f,0f).start();

                    ballFiveAnim();
                }
            }

            @Override
            public void onPause(Animator animator) { }

            @Override
            public void onResume(Animator animator) { }
        });
        av.start();
    }

    private void ballFiveAnim(){
        AnimatorValue av = new AnimatorValue();
        av.setDuration(DURATION);
        av.setDelay(DELAY);
        av.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                cradleBallFive.setRotation(AnimatorValueUtils.getAnimValues(v, -DEGREE, 0));
                cradleBallFive.setPivotX(AnimatorValueUtils.getAnimValues(v, PIVOT_X, PIVOT_X));
                cradleBallFive.setPivotY(AnimatorValueUtils.getAnimValues(v, -PIVOT_Y, -PIVOT_Y));
            }
        });
        av.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) { }

            @Override
            public void onStop(Animator animator) { }

            @Override
            public void onCancel(Animator animator) { }

            @Override
            public void onEnd(Animator animator) {
                if(isStart){
                    AnimatorClass.ofFloat(cradleBallTwo, AnimatorTypes.MOVE_BY_X, DURATION/3, 0f,-10f,0f,10f,0f,-10f,0f,10f,0f).start();
                    AnimatorClass.ofFloat(cradleBallThree, AnimatorTypes.MOVE_BY_X, DURATION/3, 0f,-10f,0f,10f,0f,-10f,0f,10f,0f).start();
                    AnimatorClass.ofFloat(cradleBallFour, AnimatorTypes.MOVE_BY_X, DURATION/3, 0f,-10f,0f,10f,0f,-10f,0f,10f,0f).start();

                    ballOneAnim();
                }
            }

            @Override
            public void onPause(Animator animator) { }

            @Override
            public void onResume(Animator animator) { }
        });
        av.start();
    }

    public void start() {
        if (!isStart) {
            isStart = true;
            ballOneAnim();
        }
    }

    public void stop() {
        isStart = false;
    }

    public boolean isStart() {
        return isStart;
    }

    public void setLoadingColor(int color) {
        cradleBallOne.setLoadingColor(color);
        cradleBallTwo.setLoadingColor(color);
        cradleBallThree.setLoadingColor(color);
        cradleBallFour.setLoadingColor(color);
        cradleBallFive.setLoadingColor(color);
    }
}
